#include <stdbool.h>
#include <sys/stat.h>

typedef struct fileinfo_t {
  int fd;
  struct stat st;
  char *con;
  bool copy_con;
} fileinfo;

int get_file_info(char *fname, fileinfo *info);

int copy_file(fileinfo *src, char *dest_name);