#include <errno.h>
#include <fcntl.h>
#include <selinux/selinux.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

typedef struct fileinfo_t {
  int fd;
  struct stat st;
  char *con;
  bool copy_con;
} fileinfo;

int get_file_info(char *fname, fileinfo *info) {
  // Open the source file
  info->fd = open(fname, O_RDONLY);
  if (info->fd == -1) {
    return -1;
  }

  // Get the file status of the source file
  if (fstat(info->fd, &info->st) != 0) {
    close(info->fd);
    return -2;
  }

  // Get the SELinux context of the source file (if available)
  int len = fgetfilecon(info->fd, &info->con);
  if (len == -1) {
    info->copy_con = false;
  } else {
    info->copy_con = true;
  }

  return 0;
}

int copy_file(fileinfo *src, char *dest_name) {
  int ret = 0;

  // Open destination file
  int dest_fd = open(dest_name, O_WRONLY | O_CREAT);
  if (dest_fd == -1) {
    ret = -1;
    goto retu;
  }

  // Copy the contents of the source file to the destination file using sendfile()
  size_t amount_left = src->st.st_size;

  while (amount_left > 0) {
    ssize_t amount_sent =
        sendfile(dest_fd, src->fd, NULL, src->st.st_size);
    if (amount_sent == -1) {
      ret = -2;
      goto closedest;
    }

    amount_left -= amount_sent;
  }  

  // Set the owner, group, and permissions of the destination file to match the
  // source file
  fchown(dest_fd, src->st.st_uid, src->st.st_gid);
  fchmod(dest_fd, src->st.st_mode);

  // Copy SELinux context (if available)
  if (src->copy_con) {
    // fprintf(stdout, "Context of %s: %s with length %d\n", argv[1], con, len);
    int res = fsetfilecon(dest_fd, src->con);
    if (res == -1) {
      ret = -3;
      goto closedest;
    }
  }

  return ret;

  closedest:
    close(dest_fd);
  retu:
    return ret;
}