#include <errno.h>
#include <selinux/selinux.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "filecpy.h"

int main(int argc, char *argv[]) {
  // Check if the user provided the source and destination file paths
  if (argc != 3) {
    fprintf(stderr, "Usage: %s <source_file> <dest_file>\n", argv[0]);
    exit(1);
  }

  fileinfo src;
  int res = get_file_info(argv[1], &src);
  if (res < 0) {
    fprintf(stderr, "get_file_info() returned an error: %d\n", errno);
    exit(EXIT_FAILURE);
  }

  res = copy_file(&src, argv[2]);
  if (res < 0) {
    fprintf(stderr, "copy_file() returned an error: %d\n", errno);
    close(src.fd);
    exit(EXIT_FAILURE);
  }

  // Free context of source file (if necessary)
  if (src.copy_con) {
    freecon(src.con);
  }

  // Close the source file
  close(src.fd);

  return 0;
}