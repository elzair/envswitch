#include <bsd/string.h>
#include <errno.h>
#include <grp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <unistd.h>

#include "procread.h"

long get_max_groups() {
    FILE *fp = fopen("/proc/sys/kernel/ngroups_max", "r");
    if (fp == NULL) {
        return -1;
    }

    long ngroups_max = 0;
    fscanf(fp, "%ld", &ngroups_max);
    if (errno) {
        int err = errno;
        fclose(fp);
        errno = err;
        return -1;
    }

    fclose(fp);
    return ngroups_max;
}

mygrp *get_groups(long nmax_groups, int *numgroups) {
    int err = 0;

    // Allocate space for ngroups_max # groups.
    size_t gsiz = (size_t)nmax_groups * sizeof(gid_t);
    gid_t *gids = (gid_t *)malloc(gsiz);
    if (gids == NULL) {
        goto ret;
    }

    // Retrieve GIDs of all groups the effective user belongs to.
    *numgroups = getgroups(gsiz, gids);
    if (*numgroups == -1) {
        goto freegids;
    }
    *numgroups += 1;

    // Trim down the amount of space used to store the GIDs.
    gsiz = (size_t)*numgroups * sizeof(gid_t);
    gids = (gid_t *)realloc(gids, gsiz);
    if (errno == ENOMEM) {
        goto freegids;
    }

    // getgroups() may or may not return the EGID, so get that specially.
    gids[*numgroups-1] = getegid();

    mygrp *groups = (mygrp *)malloc((size_t)*numgroups * sizeof(mygrp));
    if (groups == NULL) {
        goto freegids;
    }

    // Retrieve the name of the group from the GID.
    int i = 0;
    for (; i<*numgroups; i++) {
        errno = 0;
        struct group *g = getgrgid(gids[i]);
        if (g == NULL) {
            goto freegroups;
        }

        size_t namelen = (strlen(g->gr_name)+1)*sizeof(char);
        groups[i].name = (char *)malloc(namelen);
        bzero(groups[i].name, namelen);
        strlcpy(groups[i].name, g->gr_name, namelen);

        groups[i].gid = gids[i];
    }

    return groups;

    freegroups:
        err = errno;
        for (int j=0; j<=i; j++) {
            free(groups[i].name);
        }
        free(groups);
        errno = err;
    freegids:
        err = errno; 
        free(gids);
        errno = err;
    ret: return NULL;
}

bool is_admin(mygrp *groups, int numgroups) {
    char adminstr[] = "admin";

    for (int i=0; i<numgroups; i++) {
        if (strcmp(groups[i].name, adminstr) == 0) {
            return true;
        }
    }

    return false;
}