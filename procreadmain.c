#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "procread.h"

int main(int argc, char **argv) {
    long ngroups_max = get_max_groups();
    if (ngroups_max == -1) {
        fprintf(stderr, "get_max_groups() returned error: %d!\n", errno);
        exit(-1);
    }
    fprintf(stdout, "Max # Groups: %ld\n", ngroups_max);

    int numgroups = 0;
    mygrp *groups = get_groups(ngroups_max, &numgroups);
    if (groups == NULL) {
        fprintf(stderr, "get_groups() returned error: %d!\n", errno);
        exit(-1);
    }

    for (int i=0; i<numgroups; i++) {
        fprintf(
            stdout, 
            "Groups: %d %s (Length: %ld)\n", 
            groups[i].gid, 
            groups[i].name, 
            strlen(groups[i].name)
        );
    }

    if (is_admin(groups, numgroups)) {
        fprintf(stdout, "User is admin\n");
    } else {
        fprintf(stdout, "User is not admin\n");
    }

    for (int i=0; i<numgroups; i++) {
        free(groups[i].name);
    }
    free(groups);

    int res = seteuid(0);
    if (res != 0) {
        fprintf(stderr, "setuid() returned %d\n", errno);
    } else {
        printf("UID supposedly set\n");
    }

    uid_t uid = getuid();
    uid_t euid = geteuid();
    printf("Real: %d\tEffective: %d\n", uid, euid);

    res = mkdir("/var/lib/envswitch", 0700);
    if (res == -1) {
        fprintf(stderr, "mkdir() returned %d\n", errno);
    }

    return 0;
}