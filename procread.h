#pragma once

#include <grp.h>
#include <stdbool.h>

typedef struct mygrp_t {
    char *name;
    gid_t gid;
} mygrp;

long get_max_groups();

mygrp *get_groups(long nmax_groups, int *numgroups);

bool is_admin(mygrp *groups, int numgroups);